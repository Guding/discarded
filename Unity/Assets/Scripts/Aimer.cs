﻿using UnityEngine;
using System.Collections;

public class Aimer : Bolt.EntityBehaviour<IPlayerState> {

	//Target the player is looking at;
	public Transform lookTarget;

	//Target the player is aiming at.
	public Transform aimTarget;
	// Update is called once per frame
	void Update () {

		Vector3 previousRotation = transform.eulerAngles;
		previousRotation.x = state.Pitch;
		previousRotation.y = state.Yaw;
		transform.rotation = Quaternion.Euler(previousRotation);

		//lookTarget.position = TargetPosition ();
	}

	Vector3 TargetPosition(){

		RaycastHit hit;
		
		if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit)){

			return hit.point;
		}else{

			return transform.TransformDirection(Vector3.forward)*1000f;
		}
	}
}
