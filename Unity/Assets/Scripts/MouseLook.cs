﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {
	
	public Transform normalCamera;
	public Transform aimer;

	public bool riftEnabled;

	public float pitchUpClamp = -90f;
	public float pitchDownClamp = 90f;

	public float rotationY = 0f;

	void Awake ()
	{
		normalCamera = GameObject.FindWithTag("Player Camera").transform;
		if(riftEnabled){

			normalCamera.gameObject.SetActive(false);
		}
	}

	public void RotateCamera(float yaw, float pitch){

		transform.rotation = Quaternion.Euler(0, yaw, 0);

		if(!riftEnabled){
	
			normalCamera.transform.localRotation = Quaternion.Euler(pitch, 0, 0);
		}
	}
}
