﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RagdollHandler : MonoBehaviour {

	public List<Rigidbody> rigidBodies;
	public List<Collider> colliders;

	public Animator animator;

	void Awake(){

		animator = GetComponent<Animator>();

		rigidBodies = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());
		colliders = new List<Collider>(GetComponentsInChildren<Collider>());

		EnableRagdoll(false);
	}

	public void EnableRagdoll(bool enable){

		foreach(Rigidbody body in rigidBodies){

			body.isKinematic = !enable;
		}

		foreach(Collider collider in colliders){

			collider.enabled = enable;
		}
	}

	void Update(){

		if(animator.GetBool("Dead")){

			EnableRagdoll(true);

			this.enabled = false;
		}
	}
}
