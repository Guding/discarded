﻿using UnityEngine;
using System.Collections;
using Rewired;

public class PlayerInput : MonoBehaviour {

	private Player rewiredPlayer;

	public struct InputKeys{

		//Movement Inputs.
		public Vector2 movement;
		public bool jump;
		public bool run;
		public bool sprint;

		//UI Inputs.
		public bool spawnMenu;

		public bool mainMenu;
		public bool submit;
		public bool cancel;

		//Weapon Input.
		public bool unarmed;
		public bool weapon1;
		public bool weapon2;

		public float switchWeapon;

		//Shooting.
		public bool aim;
		public bool useRightHand;
	}

	//Mouse Look.
	public float yaw;
	public float pitch;

	public Vector2 movement;

	public InputKeys currentInput = new InputKeys();

	// Mouse Input
	public float XSensitivity = 2.5f;
	public float YSensitivity = 2.5f;

	public float pitchUpClamp = -80f;
	public float pitchDownClamp = 80f;

	public bool smooth;
	public float smoothtime;
	
	void Awake (){
		
		rewiredPlayer = ReInput.players.GetPlayer(0);
	}

	void Update(){

		currentInput = PollKeys ();
		movement = currentInput.movement;
	}

	public InputKeys PollKeys(){

		InputKeys newInput = new InputKeys();

		yaw += rewiredPlayer.GetAxis("Jaw")*XSensitivity;

		//Caps both yaw and pitch to a value between 0 and 360;
		if(yaw>360){

			yaw-=360;
		}else if(yaw<0){

			yaw+=360;
		}

		pitch -= rewiredPlayer.GetAxis("Pitch")*YSensitivity;

		pitch = Mathf.Clamp(pitch, pitchUpClamp, pitchDownClamp);

		newInput.movement.y = rewiredPlayer.GetAxis("Vertical");
		newInput.movement.x = rewiredPlayer.GetAxis("Horizontal");

		newInput.jump = rewiredPlayer.GetButton("Jump");
		newInput.run = rewiredPlayer.GetButton("Run");
		newInput.sprint = rewiredPlayer.GetButtonDoublePressHold("Run");

		if(rewiredPlayer.GetButtonUp("Main Menu")){
		
			newInput.mainMenu =!currentInput.mainMenu;
		}else{

			newInput.mainMenu = currentInput.mainMenu;
		}
		if(rewiredPlayer.GetButtonUp("Spawn Menu")){
			
			newInput.spawnMenu =!currentInput.spawnMenu;
		}else{

			newInput.spawnMenu = currentInput.spawnMenu;
		}

		newInput.submit = rewiredPlayer.GetButtonUp("Submit");
		newInput.cancel = rewiredPlayer.GetButtonUp("Cancel");


		newInput.unarmed = rewiredPlayer.GetButton("Unarmed");
		newInput.weapon1 = rewiredPlayer.GetButton("Weapon1");
		newInput.weapon2 = rewiredPlayer.GetButton("Weapon2");

		newInput.switchWeapon = rewiredPlayer.GetAxis("Switch Weapon");

		newInput.aim = rewiredPlayer.GetButton("Aim");
		newInput.useRightHand = rewiredPlayer.GetButton("Use Right Hand");

		return newInput;
	}
}
