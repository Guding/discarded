﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FootstepManager : MonoBehaviour {

	[System.Serializable]
	public class SurfaceType{

		public string name;
		public Texture2D[] textures;
		public AudioClip[] clips;
	}

	public AudioSource audioSource;
	public LayerMask layerMask;

	public SurfaceType[] surfaces;

	private Transform currentTransform;
	private Texture currentTexture;

	void Update(){

		RaycastHit groundHit;

		if(Physics.Raycast(transform.position, Vector3.down, out groundHit, 2f, layerMask)){

			currentTransform = groundHit.transform;
		}else{

			currentTransform = null;
		}
	}

	public void PlayFootstep(){

		//Debug.Log("Playing sound");
		if(currentTransform==null){

			return;
		}

		Terrain terrain = currentTransform.GetComponent<Terrain>();

		if(terrain!=null){

			currentTexture = GetGroundTexture(transform.position, terrain);
			//Debug.Log (terrain.name);
		}else{

			currentTexture = currentTransform.GetComponent<Renderer>().material.mainTexture;
		}

		if(currentTexture==null){

			return;
		}else{

			foreach(SurfaceType surface in surfaces){

				foreach(Texture texture in surface.textures){

					if(currentTexture==texture){

						float pitch = Random.Range(1f, 1.5f);
						AudioClip clip = surface.clips[Random.Range(0, surface.clips.Length-1)];

						audioSource.clip = clip;
						audioSource.pitch = pitch;
						audioSource.Play();

						//Debug.Log ("Played sound");
						return;
					}
				}
			}

			Debug.Log ("Found no matching textures!");
		}
	}

	private Texture2D GetGroundTexture(Vector3 worldPosition, Terrain terrain){

		TerrainData terrainData = terrain.terrainData;
		Vector3 terrainPos = terrain.transform.position;
		
		// calculate which splat map cell the worldPos falls within (ignoring y)
		int mapX = (int)(((worldPosition.x - terrainPos.x) / terrainData.size.x) * terrainData.alphamapWidth);
		int mapZ = (int)(((worldPosition.z - terrainPos.z) / terrainData.size.z) * terrainData.alphamapHeight);
		
		// get the splat data for this cell as a 1x1xN 3d array (where N = number of textures)
		float[,,] splatmapData = terrainData.GetAlphamaps(mapX,mapZ,1,1);
		
		// extract the 3D array data to a 1D array:
		float[] mix = new float[splatmapData.GetUpperBound(2)+1];
		for (int n=0; n<mix.Length; ++n)
		{
			mix[n] = splatmapData[0,0,n];    
		}
		
		float maxMix = 0;
		int maxIndex = 0;
		
		// loop through each mix value and find the maximum
		for (int n=0; n<mix.Length; ++n)
		{
			if (mix[n] > maxMix)
			{
				maxIndex = n;
				maxMix = mix[n];
			}
		}
		
		return terrainData.splatPrototypes[maxIndex].texture;
	}
}
