﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]
public class PlayerMotor : MonoBehaviour {
	
	public struct State {
		public Vector3 position;
		public Vector3 velocity;
		public Vector3 movement;
		public bool isGrounded;
		public int jumpFrames;

		//Animation states.
		public float speedBlend;
		public float direction;
	}
	
	State _state;
	public CharacterController _cc;
	public Transform _aimer;

	public float skinWidth = 0.08f;

	//Jump Variables.
	public float gravityForce = -9.81f;
	public float jumpForce = +40f;
	public int jumpTotalFrames = 30;

	//Speed variables
	public float baseAcceleration = 1f;
	public float deaccelerationModifier = 2f;

	public float baseSpeed = 2f;
	public float runSpeedModifier = 2f;
	public float sprintSpeedModifier = 2f;
	public float speedModifier = 1f;

	public float backwardsModifier = 2f;

	public float maxVelocity = 32f;

	//Rotation.
	public float idleYawDifference = 25f;
	public float maxYawOvershoot = 10f;
	public float rotationSpeed = 1f;

	public Vector3 drag = new Vector3(1f, 0f, 1f);

	public LayerMask layerMask;
	
	Vector3 sphere {
		get {
			Vector3 p;
			
			p = transform.position;
			p.y += _cc.radius;
			p.y -= (skinWidth * 2);
			
			return p;
		}
	}
	
	Vector3 waist {
		get {
			Vector3 p;
			
			p = transform.position;
			p.y += _cc.height / 2f;
			
			return p;
		}
	}
	
	public bool jumpStartedThisFrame {
		get {
			return _state.jumpFrames == (jumpTotalFrames - 1);
		}
	}
	
	void Awake() {
		_cc = GetComponent<CharacterController>();
		_state = new State();
		_state.position = transform.localPosition;
	}
	
	public void SetState(Vector3 position, Vector3 velocity, Vector3 movement, bool isGrounded, int jumpFrames) {
		// assign new state
		_state.position = position;
		_state.velocity = velocity;
		_state.movement = movement;
		_state.jumpFrames = jumpFrames;
		_state.isGrounded = isGrounded;
		
		// assign local position
		transform.localPosition = _state.position;
	}
	
	void Move(Vector3 velocity) {
		bool isGrounded = false;
		
		isGrounded = isGrounded || _cc.Move(velocity * BoltNetwork.frameDeltaTime) == CollisionFlags.Below;
		isGrounded = isGrounded || _cc.isGrounded;
		isGrounded = isGrounded || Physics.CheckSphere(sphere, _cc.radius, layerMask);
		
		if (isGrounded && !_state.isGrounded) {

			_state.velocity.x = 0f;
			_state.velocity.z = 0f;

			_state.velocity.y = gravityForce;
		}
		
		_state.isGrounded = isGrounded;
		_state.position = transform.localPosition;
	}
	
	public State Move(Vector2 movement, bool jump, bool run, bool sprint, float yaw, bool aiming) {
		bool moving = false;
		float maxSpeed = baseSpeed;
		float potentialMaxSpeed = baseSpeed*runSpeedModifier*speedModifier*sprintSpeedModifier;
		float acceleration = baseAcceleration;

		Vector3 moveDir = Vector3.zero;

		maxSpeed*=speedModifier;
		acceleration*=speedModifier;
		
		if (movement.x != 0 || movement.y != 0) {
			moveDir.z = movement.y;
			moveDir.x = movement.x;

			moving = true;
			moveDir = Vector3.Normalize(Quaternion.Euler(0, yaw, 0) * moveDir);

			if(run){
				
				maxSpeed*=runSpeedModifier;
				acceleration*=runSpeedModifier;
				
				if(sprint){
					
					maxSpeed*=sprintSpeedModifier;
					acceleration*=sprintSpeedModifier;
				}
			}

			if((movement.y<0)){

				maxSpeed/=backwardsModifier;
				potentialMaxSpeed/=backwardsModifier;

				if(sprint){

					maxSpeed/=sprintSpeedModifier;
				}
			}
		}

		if (_state.isGrounded) {

			if(moving){

				_state.movement += moveDir*acceleration*BoltNetwork.frameDeltaTime;

				if(_state.movement.magnitude>maxSpeed){
					
					_state.movement = _state.movement.normalized*maxSpeed;
				}

			}else{

				_state.movement -= _state.movement.normalized*acceleration*deaccelerationModifier*BoltNetwork.frameDeltaTime;

				if(_state.movement.magnitude<maxSpeed/4f){

					_state.movement = Vector2.zero;
				}
			}
			//Debug.Log (_state.movement.magnitude);
			_state.speedBlend = _state.movement.magnitude/potentialMaxSpeed*3f;

			if (jump && _state.jumpFrames == 0) {
				_state.jumpFrames = (byte)jumpTotalFrames;
				_state.velocity += _state.movement;
			}
			
			if (moving && _state.jumpFrames == 0) {
				Move(_state.movement);
			}else if(_state.movement.magnitude!=0){

				Move(_state.movement);
			}
		}else {

			_state.velocity.y += gravityForce * BoltNetwork.frameDeltaTime;
		}
		
		if (_state.jumpFrames > 0) {
			// calculate force
			float force;
			force = (float)_state.jumpFrames / (float)jumpTotalFrames;
			force = jumpForce * force;
			
			Move(new Vector3(0, force, 0));
		}
		
		// decrease jump frames
		_state.jumpFrames = Mathf.Max(0, _state.jumpFrames - 1);
		
		// clamp velocity
		_state.velocity = Vector3.ClampMagnitude(_state.velocity, maxVelocity);
		
		// apply drag
		_state.velocity.x = ApplyDrag(_state.velocity.x, drag.x);
		_state.velocity.y = ApplyDrag(_state.velocity.y, drag.y);
		_state.velocity.z = ApplyDrag(_state.velocity.z, drag.z);
		
		// this might seem weird, but it actually gets around a ton of issues - we basically apply 
		// gravity on the Y axis on every frame to simulate instant gravity if you step over a ledge
		_state.velocity.y = Mathf.Min(_state.velocity.y, gravityForce);
		
		// apply movement
		Move(_state.velocity);
		
		// Get aimer rotation
		Vector3 aimerRotation = _aimer.eulerAngles;
		aimerRotation.y = yaw;

		// Get body rotation;
		Vector3 bodyRotation = transform.eulerAngles;

		// Get the difference in yRotation between body and aimer.
		float yawDifference = aimerRotation.y-bodyRotation.y;

		// Corrects yawDifference to negative axis values.
		if(yawDifference>180){

			yawDifference-=360;
		}else if(yawDifference<-180){

			yawDifference+=360;
		}

		//Debug.Log (yawDifference);

		// Sets the amount to correct bodyrotation based on if we are moving or not.
		float yawOffset = 0f;


		if(moving||aiming){
			
			yawOffset = yawDifference;

			//Sets the amount to turn.
			yawOffset = Mathf.Clamp(yawOffset, -(maxYawOvershoot+idleYawDifference), (maxYawOvershoot+idleYawDifference));
			_state.direction = yawOffset/(maxYawOvershoot+idleYawDifference);

		}else{
			if(Mathf.Abs(yawDifference)>idleYawDifference){

				if(yawDifference>0){

					yawOffset=yawDifference-idleYawDifference;
				}else{
					yawOffset=yawDifference+idleYawDifference;
				}
			}

			//Sets the amount to turn.
			yawOffset = Mathf.Clamp(yawOffset, -maxYawOvershoot, maxYawOvershoot);
			_state.direction = yawOffset/maxYawOvershoot;
		} 

		

		//Debug.Log (_state.direction);
		// If we are at the maximum tolerable offset rotate the body based on offset and aimer rotation.
		if(Mathf.Abs(_state.direction)==1){

			bodyRotation.y = aimerRotation.y-(idleYawDifference+maxYawOvershoot)*_state.direction;

			bodyRotation.y += _state.direction*rotationSpeed;

		// Else rotate based on the rotationSpeed * the amount to turn.
		}else{

			bodyRotation.y += _state.direction*rotationSpeed;
		}

		//Sets body rotation;
		transform.rotation = Quaternion.Euler(bodyRotation);

		//Corrects aimer rotation.
		_aimer.rotation = Quaternion.Euler(aimerRotation);
		
		// detect tunneling
		DetectTunneling();
		
		// update position
		_state.position = transform.localPosition;


		//Debug.Log (_state.movement.magnitude);
		// done
		return _state;
	}
	
	float ApplyDrag(float value, float drag) {
		if (value < 0) {
			return Mathf.Min(value + (drag * BoltNetwork.frameDeltaTime), 0f);
		}
		
		else if (value > 0) {
			return Mathf.Max(value - (drag * BoltNetwork.frameDeltaTime), 0f);
		}
		
		return value;
	}
	
	void DetectTunneling() {
		RaycastHit hit;
		
		if (Physics.Raycast(waist, Vector3.down, out hit, _cc.height / 2, layerMask)) {
			transform.position = hit.point;
		} 
	}
	
	void OnDrawGizmos() {
		if (Application.isPlaying) {
			Gizmos.color = _state.isGrounded ? Color.green : Color.red;
			Gizmos.DrawWireSphere(sphere, _cc.radius);
			
			Gizmos.color = Color.magenta;
			Gizmos.DrawLine(waist, waist + new Vector3(0, -(_cc.height / 2f), 0));
		}
	}
}
