﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public bool debug;
	public Transform _aimer;
	public PlayerInput _input;
	public bool hasCharacter;

	public SpawnMenu spawnMenu;

	void Awake(){

		spawnMenu = GetComponentInChildren<SpawnMenu>();
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		_input = GameObject.FindWithTag("Input Handler").GetComponent<PlayerInput>();
	}

	void Update () {

		if(spawnMenu.active){

			if(!_input.currentInput.spawnMenu){

				spawnMenu.Enable(false);
			}

		}else{

			if(_input.currentInput.spawnMenu){

				spawnMenu.Enable(true);
			}
		}

		int layermask = 1<<8;
		layermask = ~layermask;
		RaycastHit hit;
		if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 100f, layermask)){

			GameObject hitObject = hit.transform.gameObject;

			if(hitObject.tag=="UI Element"){

				//hitObject.GetComponent<UIElement>()
			}
		}
	}
}
