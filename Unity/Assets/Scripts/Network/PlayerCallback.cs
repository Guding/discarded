﻿using UnityEngine;
using System.Collections;

[BoltGlobalBehaviour]
public class PlayerCallback : Bolt.GlobalEventListener {
	

	public override void ControlOfEntityGained (BoltEntity entity){

		GameObject playerCamera = GameObject.FindWithTag("Player Camera");

		entity.GetComponent<PlayerController>().SetCamera(playerCamera);
	}

	public override void ControlOfEntityLost(BoltEntity entity){

		if(!entity.isOwner){
			GameObject playerCamera = GameObject.FindWithTag("Player Camera");

			playerCamera.GetComponent<PlayerCamera>().hasCharacter = false;
			playerCamera.GetComponent<PlayerCamera>().spawnMenu.timeToRespawn = 15f;
			playerCamera.transform.parent = null;
		}
	}
}
