﻿using UnityEngine;
using System.Collections;

[BoltGlobalBehaviour(BoltNetworkModes.Server)]
public class ServerCallback : Bolt.GlobalEventListener {

	public SpawnHandler spawnHandler;

	void Awake(){

		PlayerObjectRegistry.CreateServerPlayer();
	}

	public override void Connected (BoltConnection connection){

		PlayerObjectRegistry.CreateClientPlayer(connection);
	}
	
	public override void SceneLoadLocalDone(string map){

		spawnHandler = GameObject.FindWithTag("Spawn Handler").GetComponent<SpawnHandler>();
		//PlayerObjectRegistry.serverPlayer.Spawn(GetSpawnPosition (0));
	}

	public override void SceneLoadRemoteDone(BoltConnection connection){

		//PlayerObjectRegistry.GetPlayer(connection).Spawn(GetSpawnPosition(0));
	}

	private Vector3 GetSpawnPosition(int spawnLocation){
		
		return spawnHandler.spawnPoints[spawnLocation].SpawnPosition();
	}

	public override void OnEvent (Spawn evnt){

		if(evnt.FromSelf){

			PlayerObjectRegistry.serverPlayer.Spawn(GetSpawnPosition(evnt.Location));
		}else{

			PlayerObjectRegistry.GetPlayer(evnt.RaisedBy).Spawn(GetSpawnPosition(evnt.Location));
		}
	}

	public override void OnEvent(Kill evnt){

		if(evnt.FromSelf){
			
			PlayerObjectRegistry.serverPlayer.Kill ();
		}else{
			
			PlayerObjectRegistry.GetPlayer(evnt.RaisedBy).Kill ();
		}
	}
}
