﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class PlayerObjectRegistry{

	static List<PlayerObject> players = new List<PlayerObject>();

	static PlayerObject CreatePlayer(BoltConnection connection){

		PlayerObject p;

		p = new PlayerObject();
		p.connection = connection;

		if(p.connection!=null){

			p.connection.UserData = p;
		}

		players.Add(p);

		return p;
	}

	public static IEnumerable<PlayerObject> allPlayers{

		get{return players;}
	}

	public static PlayerObject serverPlayer{

		get{return players.First(x=>x.isServer);}
	}

	public static PlayerObject CreateServerPlayer(){

		return CreatePlayer(null);
	}

	public static PlayerObject CreateClientPlayer(BoltConnection connection){

		return CreatePlayer (connection);
	}

	public static PlayerObject GetPlayer(BoltConnection connection){

		if(connection==null){

			return serverPlayer;
		}

		return (PlayerObject)connection.UserData;
	}
}
