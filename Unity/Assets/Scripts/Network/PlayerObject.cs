﻿using UnityEngine;
using System.Collections;

public class PlayerObject{

	public BoltEntity character;
	public BoltConnection connection;

	public bool isServer{
		get{return connection==null;}
	}

	public bool isClient{
		get{return connection!=null;}
	}

	public void Spawn(Vector3 spawnPosition){

		if(!character){

			character = BoltNetwork.Instantiate(BoltPrefabs.RagdollTest);	

			IPlayerState state = character.GetState<IPlayerState>();

			state.Dead = false;
			state.Health = 100;

			if(isServer){

				character.TakeControl();
			}else{

				character.AssignControl(connection);
			}

			character.transform.position = spawnPosition;

			SpawnEnemies(character.transform.position);
		}else{

			IPlayerState state = character.GetState<IPlayerState>();

			if(state.Dead&&state.RespawnFrame <= BoltNetwork.serverFrame){
				
				character = null;

				Spawn (spawnPosition);
			}
		}
	}

	public void SpawnEnemies(Vector3 playerPosition){

		int numberOfEnemies = Random.Range(3, 5);

		for(int i = 0; i<numberOfEnemies; i++){

			BoltEntity enemy;

			enemy = BoltNetwork.Instantiate(BoltPrefabs.Insect);

			enemy.transform.position = EnemyPosition(playerPosition, 50f, 25f);
			enemy.GetComponent<AIController>().basePoint = playerPosition;
			enemy.GetComponent<AIController>().UpdateWanderPoint();
		}
	}

	public Vector3 EnemyPosition(Vector3 origin, float distance, float minDistance){

		Vector3 spawnPosition = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));

		Debug.Log (spawnPosition.normalized);
		spawnPosition = origin+spawnPosition.normalized*Random.Range(minDistance, distance);

		spawnPosition.y += 500f;
		
		RaycastHit hit;
		
		if(Physics.Raycast(spawnPosition, Vector3.down, out hit, 1000f)){
			
			spawnPosition = hit.point;
			spawnPosition.y+=5f;
		}

		return spawnPosition;
	}

	public void Kill(){

		IPlayerState state = character.GetState<IPlayerState>();

		state.Dead = true;
		state.RespawnFrame = BoltNetwork.serverFrame + 15*BoltNetwork.framesPerSecond;

		if(character.hasControl){

			GameObject playerCamera = GameObject.FindWithTag("Player Camera");

			playerCamera.GetComponent<PlayerCamera>().hasCharacter = false;
			playerCamera.GetComponent<PlayerCamera>().spawnMenu.timeToRespawn = 15f;
			playerCamera.transform.parent = null;

			character.ReleaseControl();
		}

		character.RevokeControl();
	}
}
