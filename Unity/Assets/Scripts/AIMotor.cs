﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIMotor : MonoBehaviour {

	public CharacterController _cc;

	public float baseSpeed = 1f;
	public float pursueSpeedModifier = 2f;

	public float rotationSpeed = 5f;

	public float gravityForce = -9.81f;

	void Awake(){

		_cc = GetComponent<CharacterController>();
	}

	public bool RotateTowards(Transform target){

		float rotationStep = rotationSpeed*Time.deltaTime;

		Vector3 currentPos = transform.position;
		Vector3 targetPos = target.position;

		currentPos.y = 0;
		targetPos.y = 0;

		Vector3 targetDir = targetPos-currentPos;

		Vector3 forward = transform.forward;
		forward.y = 0;

		float angle = Vector3.Angle(targetDir, forward);

		Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, rotationStep, 0.0f);

		transform.rotation = Quaternion.LookRotation(newDir);

		if(angle<=30f){

			return true;
		}else{

			return false;
		}
	}

	public bool MoveTowards(Transform target, bool isPursuing, float stopDistance){

		if(Vector3.Distance(transform.position, target.position)<=stopDistance){
			
			return true;
		}

		float speed = baseSpeed;

		if(isPursuing){

			speed*=pursueSpeedModifier;
		}

		Vector3 velocity = transform.forward*speed*BoltNetwork.frameDeltaTime;

		velocity.y = gravityForce;

		_cc.Move(velocity);

		return false;
	}
}
