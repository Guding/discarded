﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : Bolt.EntityBehaviour<IAIState> {

	public struct Target{

		public Transform transform;
		public List<AudioSource> audio;
	}

	public Vector3 basePoint;

	public float targetDistance = 25f;
	public float wanderDistance = 5f;
	public float maxWanderDistance = 50f;
	public float attackDistance = 2f;

	public float fieldOfViewAngle = 90f;

	public LayerMask targetLayer;
	public LayerMask groundLayer;

	public List<Target> targets = new List<Target>();

	public AIMotor _motor;

	public Transform currentTarget;
	public Transform wanderTarget;

	public bool canSee, canHear;

	private float timer = 0;

	void Awake(){

		_motor = GetComponent<AIMotor>();

		GameObject newTarget = new GameObject();
		wanderTarget = newTarget.transform;
	}

	void Update(){

	}

	public override void SimulateOwner(){

		if(!entity.isOwner){
			
			return;
		}
		
		if(timer == 0){
			
			UpdateTargets();
		}
		
		timer+=BoltNetwork.frameDeltaTime;
		
		if(timer>=1){
			
			timer = 0;
		}
		
		if(canSee){
			
			CanSeeTarget();
		}
		
		if(canHear){
			
			CanHearTarget();
		}

		if(currentTarget==null){

			if(_motor.RotateTowards(wanderTarget)){

				if(_motor.MoveTowards(wanderTarget, false, attackDistance)){

					UpdateWanderPoint();
				}
			}
		}else{
			
			if(_motor.RotateTowards(currentTarget)){
				
				if(_motor.MoveTowards(currentTarget, true, attackDistance)){
					
					
				}
			}
		}
	}

	public override void Attached(){

		state.Transform.SetTransforms(transform);
	}

	private void UpdateTargets(){

		//Checks if the current target is outside the search distance.
		if(currentTarget!=null){

			if(Vector3.Distance(transform.position, currentTarget.position)>targetDistance){

				currentTarget = null;
				UpdateWanderPoint();
			}
		}

		targets.Clear();

		//Adds potential targets based on max distance.
		Collider[] objectsAround = Physics.OverlapSphere(transform.position, targetDistance);
		
		foreach(Collider collider in objectsAround){
			if(IsInLayerMask(collider.gameObject)){

				Target newTarget = new Target();
				newTarget.audio = new List<AudioSource>();

				AudioSource[] sources = collider.gameObject.GetComponentsInChildren<AudioSource>();

				foreach(AudioSource source in sources){

					newTarget.audio.Add(source);
				}

				newTarget.transform = collider.transform;

				targets.Add(newTarget);
			}
		}
	}

	public void UpdateWanderPoint(){

		wanderTarget.position = NewWanderTarget(basePoint);
	}

	private Vector3 NewWanderTarget(Vector3 origin){

		Vector3 wanderPoint = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));

		wanderPoint = basePoint+(wanderPoint.normalized*Random.Range(wanderDistance, maxWanderDistance));
		wanderPoint.y+=100f;

		RaycastHit hit;

		if(Physics.Raycast(wanderPoint, Vector3.down, out hit, 200f, groundLayer)){

			wanderPoint = hit.point;
		}

		return wanderPoint;
	}

	private void CanSeeTarget(){

		if(currentTarget!=null){
			
			return;
		}

		foreach(Target target in targets){

			// The target object needs to be within the field of view of the current object
			var direction = target.transform.position - (transform.position);
			direction.y = 0;

			float angle = Vector3.Angle(direction, transform.forward);

			if (direction.magnitude < targetDistance && angle < fieldOfViewAngle * 0.5f) {

				// The hit agent needs to be within view of the current agent
				RaycastHit hit;

				if (Physics.Linecast(transform.position, target.transform.position, out hit, targetLayer)) {
					if (hit.transform==target.transform) {

						currentTarget = target.transform;
					}
				}
			}
		}
	}

	private void CanHearTarget(){

		if(currentTarget!=null){
			
			return;
		}

		foreach(Target target in targets){

			foreach(AudioSource source in target.audio){

				if(source.isPlaying){

					currentTarget = target.transform;
					return;
				}
			}
		}
	}

  	private bool IsInLayerMask(GameObject obj){

		// Convert the object's layer to a bitfield for comparison
		int objLayerMask = (1 << obj.layer);
		if ((targetLayer.value & objLayerMask) > 0)  // Extra round brackets required!
			return true;
		else
			return false;
	}
}
