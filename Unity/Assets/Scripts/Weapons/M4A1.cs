﻿using UnityEngine;
using System.Collections;

public class M4A1 : Weapon {

	public override void Fx (BoltEntity entity){

		if(trailPrefab){

			//Instantiate(trailPrefab, muzzleFlash.position, muzzleFlash.rotation);
		}

		if(fireSounds.Length!=0){

			audioSource.clip = fireSounds[Random.Range(0, fireSounds.Length-1)];
			audioSource.Play();
		}
	}
}
