﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Weapon : MonoBehaviour {

	public AudioSource audioSource;

	public GameObject shellPrefab;
	public GameObject trailPrefab;

	public Transform muzzleFlash;
	public Transform shellEjector;
	public AudioClip[] fireSounds;

	public int damage;
	public int rpm;

	public int fireFrame{

		get;
		set;
	}

	public float exitSpeed;

	public int fireIntervall{

		get{

			int rps = (rpm/60);

			return BoltNetwork.framesPerSecond/rps;
		}
	}

	public virtual void OnOwner(PlayerCommand cmd, BoltEntity entity){

		if (entity.isOwner) {
			
			PlayerController controller = entity.GetComponent<PlayerController>();
			
			// display debug
			Debug.DrawRay(muzzleFlash.position, muzzleFlash.forward);

			using (var hits = BoltNetwork.RaycastAll(new Ray(muzzleFlash.position, muzzleFlash.forward), cmd.ServerFrame)) {

				if(hits.count==0){

					return;
				}

				int closestHit = 0;
				float closestDistance = 0f;

				for (int i = 0; i < hits.count; ++i) {
		
					var hit = hits.GetHit(i);

					if(i==0){

						closestHit = i;
						closestDistance = Vector3.Distance(muzzleFlash.position, hit.body.transform.position);
						continue;
					}

					float distance = Vector3.Distance(muzzleFlash.position, hit.body.transform.position);

					if(Vector3.Distance(muzzleFlash.position, hit.body.transform.position)<closestDistance){

						closestHit = i;
						closestDistance = distance;
					}
				}

				var closestHitbox = hits.GetHit(closestHit);

				var serializer = closestHitbox.body.transform.root.GetComponent<PlayerController>();
					
				if (serializer != null) {

					serializer.ApplyDamage(controller.activeWeapon.damage, closestHitbox.hitbox.hitboxType.ToString());
				}
			}
		}
	}

	public virtual void Fx(BoltEntity entity){


	}
}
