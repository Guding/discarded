﻿using UnityEngine;
using System.Collections;

public class NetworkHandler : MonoBehaviour {

	// variables for server and client connections.
	public int port;
	public string adress;
	public string instanceName;
	public int connections;
	public string levelToLoad;

	public void StartServer(){
		
		BoltLauncher.StartServer(UdpKit.UdpEndPoint.Parse(adress+":"+port));
		
		BoltNetwork.LoadScene(levelToLoad);
	}
	
	public void JoinServer(){
		
		BoltLauncher.StartClient();
		
		BoltNetwork.Connect(UdpKit.UdpEndPoint.Parse(adress+":"+port));
	}
	
	public void Disconnect(){
		
		BoltLauncher.Shutdown();
		
		Application.LoadLevel(0);
	}

	// Functions for updating the server variables.
	public void UpdateInstanceName(string newName){
		
		instanceName = newName;
	}
	
	public void UpdateAdress(string newAdress){
		
		adress = newAdress;
	}
	
	public void UpdatePort(string newPort){
		
		port = System.Convert.ToInt32(newPort);
	}
	
	public void UpdateConnections(string newConnections){
		
		connections = System.Convert.ToInt32(newConnections);
	}
	
	public void UpdateLevel(string levelName){
		
		levelToLoad = levelName;
	}
}
