﻿using UnityEngine;
using System.Collections;

public class LocalPlayerController : MonoBehaviour {

	private PlayerMotor _motor;
	private MouseLook _mouseLook;
	private PlayerInput _input;

	public bool menuActive;

	// Use this for initialization
	void Start () {
	
		_mouseLook = GetComponent<MouseLook>();
		_input = GameObject.FindWithTag("Input Handler").GetComponent<PlayerInput>();
	}
	
	// Update is called once per frame
	void Update () {
	
		_mouseLook.RotateCamera(_input.yaw, _input.pitch);
	}
}
