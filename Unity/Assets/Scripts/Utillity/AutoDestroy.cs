﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {

	private float remaining = 0;

	public float time = 3;
	
	void Awake() {
		remaining = time;
	}
	
	void Update() {
		if ((remaining -= Time.deltaTime) <= 0) {
			Destroy(gameObject);
		}
	}
}
