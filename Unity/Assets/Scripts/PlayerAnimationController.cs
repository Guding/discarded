﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;

public class PlayerAnimationController : MonoBehaviour {

	public AimIK aim;
	public FullBodyBipedIK ik;
	public LookAtIK look;

	public Poser leftHandPoser;

	private Animator animator;

	public GameObject[] leftHandEffectors;

	public Transform[] rightHandEffectors;
	public Transform[] rightHandAimEffectors;
	public GameObject[] aimPoints;

	private IKEffector rightHand { get { return ik.solver.rightHandEffector;}}
	private IKEffector leftHand  { get { return ik.solver.leftHandEffector;}}

	public Transform _camera, _head, _rightShoulder, _target, _aimTarget, _aimContainer;


	void Start(){

		animator = GetComponent<Animator>();

		aim.Disable();
		ik.Disable();
		look.Disable();

		leftHandPoser.enabled = false;
	}

	void Update(){

		if(animator.GetFloat("Movement")<-0.1||animator.GetFloat("Movement")>0.1||animator.GetFloat("Strafe")<-0.1||animator.GetFloat("Strafe")>0.1){

			animator.SetLayerWeight(2, 0);
		}else{

			animator.SetLayerWeight(2, 1);
		}
	}

	void LateUpdate(){
	
		if(animator.GetBool("Dead")){

			animator.enabled = false;
			this.enabled = false;
		}

		//Updates the position of the right hand effectors.
		_aimContainer.position = _rightShoulder.position;
		_aimContainer.LookAt(_target);

		if(animator.GetFloat("Speed")<=1.5f){

			switch(animator.GetInteger("ActiveWeapon")){
			case 0:
				aim.solver.transform = aimPoints[0].transform;
				aim.solver.axis = new Vector3(0, 0, 1);

				for (int i = 0; i < aim.solver.bones.Length; i++) {
					if (aim.solver.bones[i].rotationLimit != null) {
						aim.solver.bones[i].rotationLimit.SetDefaultLocalRotation();
					}
				}

				aim.solver.Update();
				break;
			case 1:
				aim.solver.transform = aimPoints[1].transform;
				aim.solver.axis = new Vector3(0, 0, 1);

				for (int i = 0; i < aim.solver.bones.Length; i++) {
					if (aim.solver.bones[i].rotationLimit != null) {
						aim.solver.bones[i].rotationLimit.SetDefaultLocalRotation();
					}
				}
				
				aim.solver.Update();
				break;
			case -1:

				break;
			}
		}

		if(animator.GetBool("Aiming")&&animator.GetFloat("Speed")<=0.75){
			
			switch(animator.GetInteger("ActiveWeapon")){
				
			case 0:
				
				rightHand.target = rightHandAimEffectors[0];
				rightHand.positionWeight = 1f;
				ik.solver.Update ();
				break;
				
			case 1:
				
				rightHand.target = rightHandAimEffectors[1];
				rightHand.positionWeight = 1f;
				ik.solver.Update ();
				break;

			case -1:

				rightHand.target = null;
				rightHand.position = rightHand.bone.position;
				rightHand.positionWeight = 1f;
				ik.solver.GetLimbMapping(FullBodyBipedChain.RightArm).maintainRotationWeight = 1f;
				break;
			}
		}else if(animator.GetFloat("Speed")<=1.5f){

			switch(animator.GetInteger("ActiveWeapon")){
				
			case 0:
				
				rightHand.target = rightHandEffectors[0];
				rightHand.positionWeight = 1f;
				ik.solver.Update ();
				break;
				
			case 1:
				
				rightHand.target = rightHandEffectors[1];
				rightHand.positionWeight = 1f;
				ik.solver.Update ();
				break;
				
			case -1:
				
				rightHand.target = null;
				rightHand.position = rightHand.bone.position;
				rightHand.positionWeight = 1f;
				ik.solver.GetLimbMapping(FullBodyBipedChain.RightArm).maintainRotationWeight = 1f;
				break;
			}
		}else{

			rightHand.target = null;
			rightHand.position = rightHand.bone.position;
			rightHand.positionWeight = 1f;
			ik.solver.GetLimbMapping(FullBodyBipedChain.RightArm).maintainRotationWeight = 1f;
		}

		//Pose Left hand depending on weapon active.
		switch(animator.GetInteger("ActiveWeapon")){
		case -1:
			leftHand.positionWeight = 0f;
			leftHand.rotationWeight = 0f;
			leftHandPoser.enabled = false;
			ik.solver.leftArmChain.bendConstraint.weight = 0f;
			break;
		case 0:
			leftHand.position = leftHandEffectors[0].transform.position;
			leftHand.rotation = leftHandEffectors[0].transform.rotation;
			leftHand.positionWeight = 1f;
			leftHand.rotationWeight = 1f;
			leftHandPoser.enabled = true;
			leftHandPoser.poseRoot = leftHandEffectors[0].transform;
			ik.solver.leftArmChain.bendConstraint.weight = 1f;
			break;
		case 1:
			leftHand.position = leftHandEffectors[1].transform.position;
			leftHand.rotation = leftHandEffectors[1].transform.rotation;
			leftHand.positionWeight = 1f;
			leftHand.rotationWeight = 1f;
			leftHandPoser.enabled = true;
			leftHandPoser.poseRoot = leftHandEffectors[1].transform;
			ik.solver.leftArmChain.bendConstraint.weight = 1f;
			break;
		}

		ik.solver.Update();

		look.solver.Update();

		//Updates camera position to follow head.
		if(_camera!=null){

			_camera.position = _head.position;
			_camera.LookAt(_target);
		}
	}
}
