﻿using UnityEngine;
using System.Collections;

public class PlayerController : Bolt.EntityBehaviour<IPlayerState> {

	private PlayerInput _input;
	private PlayerMotor _motor;
	public Transform _aimer;

	public GameObject[] weapons;

	public Weapon activeWeapon {
		get { return weapons[state.ActiveWeaponIndex].GetComponent<Weapon>(); }
	}

	void Awake(){

		_input = GameObject.FindWithTag("Input Handler").GetComponent<PlayerInput>();
		_motor = GetComponent<PlayerMotor>();
	}

	void Update(){

		if (state.Dead){

			_motor._cc.enabled = false;
			this.enabled = false;
		}
	}

	public void SetCamera(GameObject playerCamera){

		playerCamera.transform.parent = transform;
		playerCamera.transform.localPosition = Vector3.zero;
		playerCamera.transform.localRotation = Quaternion.identity;

		GetComponentInChildren<PlayerAnimationController>()._camera = playerCamera.transform;
		playerCamera.GetComponent<PlayerCamera>()._aimer = _aimer;
		playerCamera.GetComponent<PlayerCamera>().hasCharacter = true;

		_input.currentInput.spawnMenu = false;
	}

	public override void Attached(){

		state.Transform.SetTransforms(transform);
		state.SetAnimator(GetComponentInChildren<Animator>());

		if(entity.isOwner){

			for(int i = 0; i<state.WeaponsArray.Length; i++){

				state.WeaponsArray[i].WeaponId = i;
				state.WeaponsArray[i].WeaponAmmo = 100;
			}
		}
		state.ActiveWeaponIndex = -1;

		state.OnFire += OnFire;
		state.AddCallback("ActiveWeaponIndex", ActiveWeaponIndexIndexChanged);

		ActiveWeaponIndexIndexChanged();
	}

	void OnFire() {

		//Play Fx.
		activeWeapon.Fx(entity);
	}
	
	public override void SimulateController (){

		IPlayerCommandInput input = PlayerCommand.Create();

		input.Movement = _input.currentInput.movement;
		input.Yaw = _input.yaw;
		input.Pitch = _input.pitch;
		input.Jump = _input.currentInput.jump;
		input.Run = _input.currentInput.run;
		input.Sprint = _input.currentInput.sprint;

		if(_input.currentInput.unarmed){

			input.WeaponSelect = -1;
		}else if(_input.currentInput.weapon1){
			
			input.WeaponSelect = 0;
		}else if(_input.currentInput.weapon2){
			
			input.WeaponSelect = 1;
		}else{

			input.WeaponSelect = -2;
		}

		input.SwitchWeapon = _input.currentInput.switchWeapon;

		input.Aim = _input.currentInput.aim;
		input.UseRightHand = _input.currentInput.useRightHand;

		entity.QueueInput(input);
	}

	public override void ExecuteCommand(Bolt.Command command, bool resetState){

		PlayerCommand cmd = (PlayerCommand)command;

		if(resetState){

			_motor.SetState(cmd.Result.Position, cmd.Result.Velocity, cmd.Result.Movement, cmd.Result.Grounded, cmd.Result.JumpFrames);
		}else{

			PlayerMotor.State motorState = _motor.Move (cmd.Input.Movement, cmd.Input.Jump, cmd.Input.Run, cmd.Input.Sprint, cmd.Input.Yaw, cmd.Input.Aim);

			cmd.Result.Position = motorState.position;
			cmd.Result.Velocity = motorState.velocity;
			cmd.Result.Movement = motorState.movement;
			cmd.Result.Grounded = motorState.isGrounded;
			cmd.Result.JumpFrames = motorState.jumpFrames;

			if(cmd.IsFirstExecution){

				AnimatePlayer(cmd);
				state.Speed = motorState.speedBlend;
				state.Direction = motorState.direction;

				state.Yaw = cmd.Input.Yaw;
				state.Pitch = cmd.Input.Pitch;

				if(cmd.Input.WeaponSelect>-2){

					state.ActiveWeaponIndex = cmd.Input.WeaponSelect;
				}
				if(cmd.Input.SwitchWeapon>0){

					state.ActiveWeaponIndex= Mathf.Clamp(state.ActiveWeaponIndex+1, -1, state.WeaponsArray.Length-1);
				}else if(cmd.Input.SwitchWeapon<0){

					state.ActiveWeaponIndex=Mathf.Clamp(state.ActiveWeaponIndex-1, -1, state.WeaponsArray.Length-1);
				}

				if(!entity.hasControl){
					//Debug.Log(cmd.Input.SwitchWeapon);
				}
	
				state.ActiveWeapon = state.ActiveWeaponIndex;

				//Shooting logic.

				state.Aiming = cmd.Input.Aim;

				if(cmd.Input.UseRightHand){

					if(state.ActiveWeaponIndex>=0){

						FireWeapon (cmd);
					}
				}
			}
		}
	}

	void FireWeapon(PlayerCommand cmd) {
		if (activeWeapon.fireFrame + activeWeapon.fireIntervall <= BoltNetwork.serverFrame) {
			activeWeapon.fireFrame = BoltNetwork.serverFrame;
			
			state.Animator.SetTrigger("Fire");//.Fire();
			
			// if we are the owner and the active weapon is a hitscan weapon, do logic
			if (entity.isOwner) {
				activeWeapon.OnOwner(cmd, entity);
			}
		}
	}

	public void ApplyDamage(int damage, string bodyPart) {

		if (!state.Dead) {
			
			state.Health -= damage;

			Debug.Log (bodyPart + ": was hit with a bullet dealing: " + damage + " damage. Player now has: " + state.Health + " health left.");

			if (state.Health > 100 || state.Health < 0) {
				state.Health = 0;
			}

			if (state.Health == 0) {
				
				Debug.Log ("Player died");
				PlayerObjectRegistry.GetPlayer(entity.controller).Kill();
			}
		}
	}

	void ActiveWeaponIndexIndexChanged() {
		for (int i = 0; i < weapons.Length; ++i) {
			weapons[i].SetActive(false);
		}
		
		if (state.ActiveWeaponIndex >= 0) {
			int objectId = state.WeaponsArray[state.ActiveWeaponIndex].WeaponId;
			weapons[objectId].SetActive(true);
		}
	}

	void AnimatePlayer(PlayerCommand cmd){

		//Movement Animation.
		state.Movement = cmd.Input.Movement.y;
		state.Strafe = cmd.Input.Movement.x;
	}
}
