﻿using UnityEngine;
using System.Collections;

public class PortInput : UIInput {

	// Use this for initialization
	void Start () {
	
		placeholder.text = _network.port.ToString();
		inputField.text = _network.port.ToString();
	}

}
