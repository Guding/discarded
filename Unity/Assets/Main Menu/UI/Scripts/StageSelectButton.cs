﻿using UnityEngine;
using System.Collections;

public class StageSelectButton : UIButton {

	public string level;

	public override void OnClick(){

		GameObject.FindWithTag("Network Handler").GetComponent<NetworkHandler>().UpdateLevel(level);
	}
}
