﻿using UnityEngine;
using System.Collections;

public class ConnectionsInput : UIInput {

	// Use this for initialization
	void Start () {
	
		placeholder.text = _network.connections.ToString();
		inputField.text = _network.connections.ToString();
	}

}
