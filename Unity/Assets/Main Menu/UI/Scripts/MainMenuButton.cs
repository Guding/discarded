﻿using UnityEngine;
using System.Collections;

public class MainMenuButton : UIButton {

	public int menuNumber;
	public UIMenu focusMenu;

	public override void OnClick(){

		transform.root.GetComponent<UINavigation>().ChangeMenu(menuNumber, focusMenu, true);
	}
}
