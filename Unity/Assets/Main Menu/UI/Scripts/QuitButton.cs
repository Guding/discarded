﻿using UnityEngine;
using System.Collections;

public class QuitButton : UIButton {

	public override void OnClick(){

		Application.Quit();
	}
}
