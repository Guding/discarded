﻿using UnityEngine;
using System.Collections;

public class JoinGameButton : UIButton {

	public override void OnClick(){

		GameObject.FindWithTag("Network Handler").GetComponent<NetworkHandler>().JoinServer();
	}
}
