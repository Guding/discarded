﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Rewired;

public class MainMenu : MonoBehaviour {
	
	public bool inMenuScene;

	public UIElement logo;
	public UINavigation navigation;

	void Start(){

		if(inMenuScene){
			logo.Enable(true);

			navigation.Enable(true);
		}
	}

}
