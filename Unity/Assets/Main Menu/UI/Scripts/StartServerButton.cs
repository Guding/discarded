﻿using UnityEngine;
using System.Collections;

public class StartServerButton : UIButton {
	
	public override void OnClick(){
		
		GameObject.FindWithTag("Network Handler").GetComponent<NetworkHandler>().StartServer();
	}
}
