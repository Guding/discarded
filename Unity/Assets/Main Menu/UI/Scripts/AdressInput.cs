﻿using UnityEngine;
using System.Collections;

public class AdressInput : UIInput {

	// Use this for initialization
	void Start () {
	
		placeholder.text = _network.adress;
		inputField.text = _network.adress;

	}
}
