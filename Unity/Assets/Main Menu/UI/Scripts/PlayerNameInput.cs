﻿using UnityEngine;
using System.Collections;

public class PlayerNameInput : UIInput {

	void Start(){

		placeholder.text = _network.instanceName;
		inputField.text = _network.instanceName;
	}
}
