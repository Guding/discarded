﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIElement))]
public class UISelection : MonoBehaviour {

	public enum scrollDirection {UP, DOWN, LEFT, RIGHT};

	public UIElement[] objects;
	public UIElement selectedObject;

	public int objectsPerRow;

	public int selected;

	public bool isActive;

	public virtual void Awake(){

		selectedObject=objects[selected];
	}

	public virtual void OnClick(){

		if(isActive){

			objects[selected].OnClick();
			transform.root.GetComponent<UINavigation>().selectionActive = false;
			isActive = false;
		}else{

			isActive = true;
			transform.root.GetComponent<UINavigation>().selectionActive = isActive;
			transform.root.GetComponent<UINavigation>().activeSelection = this;
		}
	}

	public virtual void ScrollSelection(scrollDirection direction){

		switch(direction){

		case scrollDirection.UP:

			if(selected-objectsPerRow>=0){

				objects[selected].SetFocus(false);
				selected-=objectsPerRow;
				objects[selected].SetFocus(true);
			}else if(selected!=0){

				objects[selected].SetFocus(false);
				selected=0;
				objects[selected].SetFocus(true);
			}
			break;
		case scrollDirection.DOWN:

			if(selected+objectsPerRow<objects.Length){
				
				objects[selected].SetFocus(false);
				selected+=objectsPerRow;
				objects[selected].SetFocus(true);
			}else if(selected!=objects.Length-1){

				objects[selected].SetFocus(false);
				selected=objects.Length-1;
				objects[selected].SetFocus(true);
			}
			break;

		case scrollDirection.LEFT:

			if(selected>0){

				objects[selected].SetFocus(false);
				selected--;
				objects[selected].SetFocus(true);
			}
			break;

		case scrollDirection.RIGHT:

			if(selected<objects.Length-1){
				
				objects[selected].SetFocus(false);
				selected++;
				objects[selected].SetFocus(true);
			}
			break;
		}
	}
}
