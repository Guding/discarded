﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]
public class UIMenu : MonoBehaviour {

	public List<UIElement> elements;

	public int currentFocus = 0;

	public bool allowScrolling;

	private Animator animator;

	void Awake(){

		animator = GetComponent<Animator>();
	}

	public void Enable(bool enable){

		animator.SetBool("Enabled", enable);

		foreach(UIElement element in elements){

			element.Enable(enable);
		}

		SetFocus(enable);
	}

	public void SetFocus(bool focus){

		if(elements[currentFocus].SetFocus(focus)){
			
			return;
		}else{
			
			ScrollMenu(true);
		}
	}

	public void OnClick(){

		elements[currentFocus].OnClick();
	}

	public void ScrollMenu(bool upOrDown){

		if(!allowScrolling){

			return;
		}
		elements[currentFocus].SetFocus(false);

		if(upOrDown){

			currentFocus++;

			if(currentFocus>elements.Count-1){

				currentFocus = 0;
			}
		}else{

			currentFocus--;
			if(currentFocus<0){
				
				currentFocus = elements.Count-1;
			}
		}

		if(elements[currentFocus].SetFocus(true)){

			return;
		}else{

			ScrollMenu(upOrDown);
		}
	}
}
