﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(UIElement))]
public class UIInput : MonoBehaviour {

	public NetworkHandler _network;
	public InputField inputField;
	public Text placeholder;

	public bool waitingForInput = false;

	// Use this for initialization
	public virtual void Awake () {
	
		_network = GameObject.FindWithTag("Network Handler").GetComponent<NetworkHandler>();
		inputField = GetComponentInChildren<InputField>();
		placeholder = inputField.GetComponentInChildren<Text>();
	}

	public virtual void OnFocus(bool focus){

		if(!waitingForInput){

			waitingForInput = true;
			transform.root.GetComponent<UINavigation>().inputReady = true;
			return;
		}
		transform.root.GetComponent<UINavigation>().focusInput = focus;
		transform.root.GetComponent<UINavigation>().inputReady = focus;
		waitingForInput = focus;

		if(focus) {

			inputField.Select();
			inputField.ActivateInputField();
		}else{

			inputField.DeactivateInputField();
		}
	}
}
