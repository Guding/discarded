﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UINavigation : MonoBehaviour {

	private PlayerInput _input;
	
	public UIMenu currentMenu;
	
	public bool navigationReady;
	public bool allowMenuNavigation;

	//Variables for controlling input fields and selecting them in the menu.
	public bool focusInput;
	public bool inputReady;
	public bool selectionActive;

	public UISelection activeSelection;

	public UIMenu[] menus;

	public bool active;
	
	void Start ()
	{
		_input = GameObject.FindWithTag("Input Handler").GetComponent<PlayerInput>();

	}
	
	void Update(){

		if(active){

			MenuNavigation();
		}
	}

	public void Enable(bool enable){

		active = enable;

		foreach(UIMenu menu in menus){
			
			menu.Enable(enable);
		}
	}

	public void ChangeMenu(int menuNumber, UIMenu menu, bool enable){

		if(menus[menuNumber] != menu){

			menus[menuNumber].Enable(false);
			menus[menuNumber] = menu;

			currentMenu.SetFocus(false);

			currentMenu = menus[menuNumber];

			if(enable){
				currentMenu.Enable(true);
			}
		}
	}

	void MenuNavigation(){

		//If the player has let go of the keys, enable navigation again.
		if(_input.currentInput.movement.y==0&&_input.currentInput.movement.x==0&&!_input.currentInput.jump){
			
			navigationReady = true;
		}else if(!navigationReady){
			
			return;
		}

		//Scrolling for selection fields if one is active.
		if(selectionActive){

			if(_input.currentInput.submit||_input.currentInput.cancel||_input.currentInput.jump){

				activeSelection.OnClick();
			}

			if(_input.currentInput.movement.y>0){
				
				activeSelection.ScrollSelection(UISelection.scrollDirection.UP);
				navigationReady = false;
			}else if(_input.currentInput.movement.y<0){
				
				activeSelection.ScrollSelection(UISelection.scrollDirection.DOWN);
				navigationReady = false;
			}else if(_input.currentInput.movement.x>0){
				
				activeSelection.ScrollSelection(UISelection.scrollDirection.RIGHT);
				navigationReady = false;
			}else if(_input.currentInput.movement.x<0){
				
				activeSelection.ScrollSelection(UISelection.scrollDirection.LEFT);
				navigationReady = false;
			}

			return;
		}

		//stops scrolling when input field is active.
		if(inputReady){

			if(focusInput){
				
				if(_input.currentInput.submit||_input.currentInput.cancel){
					
					currentMenu.ScrollMenu(true);
				}
				return;
			}

			if(_input.currentInput.submit){
				
				currentMenu.SetFocus(true);
				return;
			}
		}
		
		//Scrolls the menu up or down depending on the vertical axis.
		if(_input.currentInput.movement.y>0){
			
			currentMenu.ScrollMenu(false);
			navigationReady = false;
		}else if(_input.currentInput.movement.y<0){
			
			currentMenu.ScrollMenu(true);
			navigationReady = false;
		}

		if(_input.currentInput.jump||_input.currentInput.submit){

			currentMenu.OnClick();
			navigationReady = false;
		}

		//Horizontal navigation.
		if(menus.Length>1&&allowMenuNavigation){

			if(_input.currentInput.movement.x>0){

				for(int i = 0; i<menus.Length-1; i++){

					if(menus[i]==currentMenu){
						currentMenu.SetFocus(false);
						currentMenu=menus[i+1];
						currentMenu.SetFocus(true);
						navigationReady = false;
					}
				}
			}else if(_input.currentInput.movement.x<0){
				
				for(int i = menus.Length-1; i>0; i--){
					
					if(menus[i]==currentMenu){
						currentMenu.SetFocus(false);
						currentMenu=menus[i-1];
						currentMenu.SetFocus(true);
						navigationReady = false;
					}
				}
			}
		}
	}
}
