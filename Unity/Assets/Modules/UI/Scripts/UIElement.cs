﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(Animator))]
public class UIElement : MonoBehaviour {


	private Animator animator;

	public enum type {BUTTON, INPUT, LOGO, SELECTION};

	public type UIType;

	//Variable if element is a button.
	public UIButton button;

	//Variables if the element is of input type.
	public UIInput inputElement;

	//Variable if element is a selection.
	public UISelection selection;

	void Awake(){

		animator = GetComponent<Animator>();
		switch(UIType){

		case type.BUTTON:
			button = GetComponent<UIButton>();
			break;
		case type.INPUT:
			inputElement = GetComponent<UIInput>();
			break;
		case type.SELECTION:
			selection = GetComponent<UISelection>();
			break;
		}
	}

	public void Enable(bool enable){

		switch(UIType){

		case type.SELECTION:

			animator.SetBool("Enabled", enable);
			foreach(UIElement element in selection.objects){

				element.Enable(true);
			}
			selection.selectedObject.SetFocus(true);
			break;
		default:

			animator.SetBool("Enabled", enable);
			break;
		}

	}

	public bool SetFocus(bool focus){

		switch(UIType){
		case type.BUTTON:
		
			animator.SetBool("Focus", focus);
			return true;
		case type.INPUT:

			animator.SetBool("Focus", focus);

			inputElement.OnFocus(focus);

			return true;
		case type.LOGO:

			return false;
		case type.SELECTION:

			animator.SetBool("Focus", focus);
			return true;
		}

		return false;
	}

	public void OnClick(){

		switch(UIType){

		case type.BUTTON:

			button.OnClick();
			break;
		case type.SELECTION:

			selection.OnClick();
			break;
		}
	}
}
