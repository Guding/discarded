﻿using UnityEngine;
using System.Collections;

public class KillButton : UIButton {

	public override void OnClick(){

		Kill killEvent = Kill.Create(Bolt.GlobalTargets.OnlyServer);

		killEvent.Send();
	}
}
