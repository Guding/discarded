﻿using UnityEngine;
using System.Collections;

public class SpawnPointButton : UIButton {

	public int spawnLocation;
	public SpawnMenu spawnMenu;

	void Start(){

		spawnMenu = transform.root.GetComponentInChildren<SpawnMenu>();
	}

	public override void OnClick(){

		spawnMenu.SendSpawnEvent(spawnLocation);
	}
}
