﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SpawnMenu : MonoBehaviour {

	public SpawnHandler spawnHandler;

	public CanvasGroup textGroup;
	public Text respawnTimer;
	public float timeToRespawn;

	public UIElement logo;
	public UIElement respawnText;
	public UINavigation navigation;
	public UIMenu locationChooser, spawnMenu;

	public GameObject spawnPointUI;

	//Controlls diferent states of the spawn menu.
	public bool active, timerActive;

	private PlayerCamera _camera;

	void Awake(){
		
		spawnHandler = GameObject.FindWithTag("Spawn Handler").GetComponent<SpawnHandler>();
		_camera = transform.root.GetComponent<PlayerCamera>();
	}

	void Start(){

		//Loops through the array of spawn points in the spawn handler, then creates a new ui element in the spawn menu for each spawn point.
		//It takes the texture from the SpawnPoint class and applies it to the element, then sets the correct int to the spawn location variable.
		for(int i = 0; i<spawnHandler.spawnPoints.Length; i++){

			GameObject newSpawnPoint = Instantiate(spawnPointUI) as GameObject;

			newSpawnPoint.transform.parent = locationChooser.transform;
			newSpawnPoint.transform.localPosition = new Vector3(0, -i*0.35f, 0);

			locationChooser.elements.Add(newSpawnPoint.GetComponent<UIElement>());

			newSpawnPoint.GetComponent<SpawnPointButton>().spawnLocation = i;
			newSpawnPoint.GetComponent<MeshRenderer>().material.mainTexture = spawnHandler.spawnPoints[i].GetComponent<SpawnPoint>().uiTexture;
		}


	}

	void Update(){

		//When the player gets killed the timeToRespawn gets set to a number. While that number is above zero it gets counted down.
		//If the number is below zero the respawn timer text gets hidden.
		if(timeToRespawn>0){

			if(!timerActive){

				//Used to tell if we have allready enabled/disabled all components if the timer starts.
				timerActive = true;

				// simple way to fade in and out Unity GUI components.
				textGroup.alpha = 1;

				//Enables the respawn timer text.
				respawnText.Enable(true);

				//Disables the Spawn Menu since it isn't enabled while we wait for the respawn timer to tick down.
				spawnMenu.Enable(false);
			}

			//Counts down the timer and changes the text component that displays the value left.
			timeToRespawn-=Time.deltaTime;

			respawnTimer.text = Mathf.RoundToInt(timeToRespawn).ToString();

			if(timeToRespawn<=0){

				//When the timer hits <=0 disable the respawn timer again and enable the spawn menu if the Spawn UI is active.
				timerActive = false;
				textGroup.alpha = 0;
				respawnText.Enable(false);

				if(active){

					//Switches the menu element to the location chooser and enables it because the spawn ui is active.
					navigation.ChangeMenu(0, locationChooser, true);
				}
			}
		}
	}

	//Function called by the Player Camera script that enables/disables the UI.
	public void Enable(bool enable){

		active = enable;
		logo.Enable(enable);

		//Tells if the player is alive essentaially. If _camera.hasCharacter is false the player has no active body and is either dead or hasn't spawned yet.
		if(_camera.hasCharacter){

			//because we are alive the spawnmenu should be the active menu. But it shouldn't be enabled here.
			navigation.ChangeMenu(0, spawnMenu, false);

		}else{

			// if the respawn timer isn't active we can show the location chooser instead.
			if(timeToRespawn<=0){

				navigation.ChangeMenu(0, locationChooser, false);
			}
		}

		//Enables navigation and also enables all ui elements.
		navigation.Enable(enable);
	}

	
	public void SendSpawnEvent(int spawnLocation){

		Spawn spawnEvenet = Spawn.Create(Bolt.GlobalTargets.OnlyServer);

		spawnEvenet.Location = spawnLocation;
		spawnEvenet.Send();
	}
}
