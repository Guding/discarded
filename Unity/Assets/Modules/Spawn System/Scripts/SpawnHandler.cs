﻿using UnityEngine;
using System.Collections;

public class SpawnHandler : MonoBehaviour {

	public SpawnPoint[] spawnPoints;
	
	void Awake(){
		
		spawnPoints = GetComponentsInChildren<SpawnPoint>();
	}
}
